﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PayrollCliApp
{
    internal class PayCalculator
    {
        public string Location { get; private set; }
        public decimal Salary { get; private set; }
        public decimal IncomeTax { get; private set; }
        public decimal TaxableIncome { get; private set; }
        public decimal MedicareLevy { get; private set; }
        public decimal Superannuation { get; private set; }
        public decimal BudgetRepairLevy { get; private set; }
        public decimal TaxOffset { get; private set; }
        public decimal NetSalary { get; private set; }
        
        public PayCalculator(decimal salary) : this(salary, "Australia")
        {  
        }

        public PayCalculator(decimal salary, string location)
        {
            if (salary < 0)
            {
                throw new System.ArgumentException("Salary cannot be a negative value");
            } 
            
            this.Salary = salary;
            this.Location = location;
            this.Superannuation = CalculateSuper(salary);
            this.TaxableIncome = CalculateTaxableIncome(salary);
            this.IncomeTax = CalculateTax(TaxableIncome);
            this.MedicareLevy = CalculateMedicareLevy(TaxableIncome);
            this.BudgetRepairLevy = CalculateBudgetRepairLevy(TaxableIncome);
            this.TaxOffset = CalculateTaxOffset(TaxableIncome);
            this.NetSalary = Salary - Superannuation - IncomeTax - MedicareLevy - BudgetRepairLevy - TaxOffset;
        }

        public override string ToString()
        {
            return string.Format("Salary: {0:n2} \nTaxableIncome: {1:n2} \nSuperannuation: {2:n2} \nIncomeTax: {3:n2} " +
                "\nMedicareLevy: {4:n2} \nBudgetRepairLevy: {5:n2} \nTaxOffset: {6:n2} \nNetSalary: {7:n2}",
                Salary, TaxableIncome, Superannuation, IncomeTax, MedicareLevy, BudgetRepairLevy, TaxOffset, NetSalary);
        }

        internal static decimal CalculateTax(decimal salary)
        {
            if (salary < 0)
            {
                throw new System.ArgumentException("Salary cannot be a negative value");
            }
            // $54,232 plus 45c for each $1 over $180,000 for 
            // taxable income of $180,001 and over
            else if (salary > 180000)
            {
                return Math.Ceiling(54232 + (salary - 180000) * .45m);
            }
            // $19,822 plus 37c for each $1 over $87,000 for 
            // taxable income in the range of $87,001 – $180,000 p.a.
            else if (salary > 87000)
            {
                return Math.Ceiling(19822 + (salary - 87000) * .37m);
            }
            // 3,572 plus 32.5c for each $1 over $37,000 
            // for taxable income in the range of $37,001 – $87,000
            else if (salary > 37000)
            {
                return Math.Ceiling(3572 + (salary - 37000) * .325m);
            }
            // 19c for each $1 over 18,200 
            // for taxable income in the range of $18,201 – $37,000
            else if (salary > 18200)
            {
                return Math.Ceiling((salary - 18200) * .19m);
            }

            // Taxable Income <= $18,200 = NIL
            return 0;
        }


        public static decimal CalculateMedicareLevy(decimal income)
        {
            if (income < 0)
            {
                throw new System.ArgumentException("Negative Amount is not allowed");
            }
            // Taxable Income > 26668 = 2% is applied
            else if (income > 26668)
            {
                return Math.Ceiling(income * .02m);
            }
            // Taxable Income from $21,336 to $ 26,668, 
            // apply 10% over the difference of $21,335
            else if (income > 21335)
            {
                return Math.Ceiling((income - 21335) * .1m);
            }

            // Taxable Income <= 21335 = NIL
            return 0;
        }

        public static decimal CalculateSuper(decimal income)
        {
            if (income < 0)
            {
                throw new System.ArgumentException("Negative Amount is not allowed");
            }

            // A compulsory Superannuation contribution of 8.6758% is applied
            return income * .086758m;
        }

        public static decimal CalculateBudgetRepairLevy(decimal income)
        {
            if (income < 0)
            {
                throw new System.ArgumentException("Negative Amount is not allowed");
            }
            // Repair Levy of 2% on thae part of taxable income which exceeds $180,000
            else if (income > 180000)
            {
                return Math.Ceiling((income - 180000) * .02m);
            }

            return 0;
        }

        public static decimal CalculateTaxOffset(decimal income)
        {
            if (income < 0)
            {
                throw new System.ArgumentException("Negative Amount is not allowed");
            }
            // offset is $445 is applied to taxable income <= $37,000
            else if (income <= 37000)
            {
                return 445;
            }
            // For taxable income > $37.000 and < $66,667, offset is 
            // $445 reduced by 1.5 cents for each dollar over $37,000
            else if (income < 66667)
            {
                return 445 - (income - 37000) * .015m;
            }

            // eligible to TaxOffset if taxable income is less than $66,667
            return 0;
        }

        public static decimal CalculateTaxableIncome(decimal salary)
        {
            // Taxable Income is Annual Salary minus Superannuation
            return salary - CalculateSuper(salary);
        }

    }
}
