﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PayrollCliApp
{
    class PayrollCli
    {
        static void Main(string[] args)
        {
            PayrollCli cli = new PayrollCli();
            cli.execute();
        }

        public void execute()
        {
            string input;
            string country;
            decimal salary;

            Console.WriteLine("\n------------- Payroll Calculator -------------");
            Console.Write("\nPlease enter the annual salary: ");
            input = Console.ReadLine();

            bool parsed = decimal.TryParse(input, out salary);
            if (!parsed || salary < 0)
            {
                System.Console.WriteLine("\n Invalid Input: Invalid value for the Salary amoount. A numeric value is required. Exiting...");
            }
            else
            {
                Console.Write("Please enter the employee’s location: ");
                country = Console.ReadLine();

                PayCalculator payroll = new PayCalculator(salary, country);
                PaySlipPrinter.PrintToConsole(payroll);
            }
        }
    }
}
