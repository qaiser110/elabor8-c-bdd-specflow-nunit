﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PayrollCliApp
{
    class PaySlipPrinter
    {
        internal static void PrintToConsole(PayCalculator data)
        {
            Console.WriteLine("\n\t Employee location: {0:C}", data.Location);
            Console.WriteLine("\t Gross Salary: {0:C}", data.Salary);

            Console.WriteLine("\n\t Taxable Income: {0:C}", data.TaxableIncome);
            
            Console.WriteLine("\n\t Deductions:");
            Console.WriteLine("\t Superannuation: {0:C}", data.Superannuation);
            Console.WriteLine("\t Medicare Levy: {0:C}", data.MedicareLevy);
            Console.WriteLine("\t Income Tax: {0:C}", data.IncomeTax);
            Console.WriteLine("\t Temporary Budget Repair Levy: {0:C}", data.BudgetRepairLevy);

            Console.WriteLine("\n\t Net annual salary: {0:C}", data.NetSalary);

        }
    }
}
