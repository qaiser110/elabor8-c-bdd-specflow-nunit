﻿using System;
using TechTalk.SpecFlow;
using NUnit.Framework;
using PayrollCliApp;

namespace PayrollCliApp.Specs
{
    [Binding]
    public class TaxableIncomeSteps
    {
        [When(@"I calculate TaxableIncome on Salary")]
        public void WhenICalculateTaxableIncomeOnSalary()
        {
            decimal amount = (decimal)ScenarioContext.Current["amount"];
            ScenarioContext.Current["result"] = PayCalculator.CalculateTaxableIncome(amount);

        }
    }
}
