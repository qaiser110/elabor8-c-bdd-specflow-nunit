﻿using System;
using TechTalk.SpecFlow;
using NUnit.Framework;
using PayrollCliApp;

namespace PayrollCliApp.Specs
{
    [Binding]
    public class PayCalculatorSteps
    {
        private static PayCalculator _calc;

        private static object GetVal(string propName)
        {
            return _calc.GetType().GetProperty(propName).GetValue(_calc, null);
        }

        [When(@"I calculate the Payroll")]
        public void WhenICalculateThePayroll()
        {
            decimal salary = (decimal)ScenarioContext.Current["amount"];
            _calc = new PayCalculator(salary);
        }

        [Then(@"the ""(.*)"" should be (.*)")]
        public void ThenTheShouldBe(string prop, decimal expected)
        {
            Assert.That(GetVal(prop), Is.EqualTo(expected));
        }

        [When(@"I try to calculate Payroll on Salary")]
        public void WhenITryToCalculatePayrollOnSalary()
        {
            decimal salary = (decimal)ScenarioContext.Current["amount"];
            ScenarioContext.Current["error"] =
                Assert.Throws<ArgumentException>(() => new PayCalculator(salary));
        }

        [AfterScenario]
        public void AfterScenario()
        {
            Exception lastError = ScenarioContext.Current.TestError;

            if (lastError != null)
            {
                Console.Write(_calc);
            }
        }
    }
}
