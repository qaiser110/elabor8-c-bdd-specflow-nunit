﻿Feature: TaxOffset
    In order to claculate the net salary
    As a Payroll Manager
    I want to be able to calculate the TaxOffset on Taxable Income

@tax
Scenario Outline: Calculate TaxOffset on different amounts
    Given the amount of <amount>
    When I calculate TaxOffset on Taxable Income
    Then the result should be <expectedResult>

    Examples:
    | amount      | expectedResult   |
    | 66667       | 0                |
    | 8000000.99  | 0                |
    | 100         | 445              |
    | 37000       | 445              |
    | 37001       | 444.985          |
    | 66000       | 10               |

@tax
Scenario: Generate error when Amount is a negative value
    Given the amount of -1
    When I try to calculate TaxOffset on Income
    Then an error message "Negative Amount is not allowed" is received
