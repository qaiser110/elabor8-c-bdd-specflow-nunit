﻿using System;
using TechTalk.SpecFlow;
using NUnit.Framework;
using PayrollCliApp;

namespace PayrollCliApp.Specs
{
    [Binding]
    public class SuperannuationSteps
    {
        [When(@"I calculate Superannuation on Taxable Income")]
        public void WhenICalculateSuperannuationOnTaxableIncome()
        {
            decimal amount = (decimal)ScenarioContext.Current["amount"];
            ScenarioContext.Current["result"] = PayCalculator.CalculateSuper(amount);
        }

        [When(@"I try to calculate Superannuation on Income")]
        public void WhenITryToCalculateSuperannuationOnIncome()
        {
            decimal amount = (decimal)ScenarioContext.Current["amount"];
            ScenarioContext.Current["error"] =
                Assert.Throws<ArgumentException>(() => PayCalculator.CalculateSuper(amount));
        }

    }
}
