﻿Feature: TaxableIncome
    In order to claculate the net salary
    As a Payroll Manager
    I want to be able to calculate the TaxableIncome from Annual Salary and Superannuation

@tax
Scenario Outline: Calculate TaxOffset on different amounts
    Given the salary of <amount>
    When I calculate TaxableIncome on Salary
    Then the result should be <expectedResult>

    Examples:
    | amount    | expectedResult |
    | 30000     | 27397.26       |
    | 8000000.5 | 7305936.456621 |
