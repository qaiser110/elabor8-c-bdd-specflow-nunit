﻿Feature: BudgetRepairLevy
    In order to claculate the net salary
    As a Payroll Manager
    I want to be able to calculate the BudgetRepairLevy on Taxable Income

@tax
Scenario Outline: Calculate BudgetRepairLevy on different amounts
    Given the amount of <amount>
    When I calculate BudgetRepairLevy on Taxable Income
    Then the result should be <expectedResult>

    Examples:
    | amount      | expectedResult   |
    | 180000      | 0                |
    | 180001      | 1                |
    | 8999999.999 | 176400           |

@tax
Scenario: Generate error when Amount is a negative value
    Given the amount of -1
    When I try to calculate BudgetRepairLevy on Income
    Then an error message "Negative Amount is not allowed" is received
