﻿using System;
using TechTalk.SpecFlow;
using NUnit.Framework;
using PayrollCliApp;

namespace PayrollCliApp.Specs
{
    [Binding]
    public class BudgetRepairLevySteps
    {
        [When(@"I calculate BudgetRepairLevy on Taxable Income")]
        public void WhenICalculateBudgetRepairLevyOnTaxableIncome()
        {
            decimal amount = (decimal)ScenarioContext.Current["amount"];
            ScenarioContext.Current["result"] = PayCalculator.CalculateBudgetRepairLevy(amount);
        }
        
        [When(@"I try to calculate BudgetRepairLevy on Income")]
        public void WhenITryToCalculateBudgetRepairLevyOnIncome()
        {
            decimal amount = (decimal)ScenarioContext.Current["amount"];
            ScenarioContext.Current["error"] =
                Assert.Throws<ArgumentException>(() => PayCalculator.CalculateBudgetRepairLevy(amount));
        }
    }
}
