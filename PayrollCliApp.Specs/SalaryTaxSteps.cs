﻿using System;
using TechTalk.SpecFlow;
using NUnit.Framework;
using PayrollCliApp;

namespace PayrollCliApp.Specs
{
    [Binding]
    public class SalaryTaxSteps
    {
        [When(@"I calculate Tax on Salary")]
        public void WhenICalculateTaxOnSalary()
        {
            decimal amount = (decimal)ScenarioContext.Current["amount"];
            ScenarioContext.Current["result"] = PayCalculator.CalculateTax(amount);
        }
        
        [When(@"I try to calculate Tax on Salary")]
        public void WhenITryToCalculateTaxOnSalary()
        {
            decimal amount = (decimal)ScenarioContext.Current["amount"];
            ScenarioContext.Current["error"] =
                Assert.Throws<ArgumentException>(() => PayCalculator.CalculateTax(amount));
        }

    }
}
