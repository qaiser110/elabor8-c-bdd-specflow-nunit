﻿Feature: SalaryTax
	In order to claculate the net salary
	As a Payroll Manager
	I want to be able to calculate the Tax on Gross Salary

@tax
Scenario Outline: Calculate Income Tax on different Salary amounts
	Given the amount of <amount>
	When I calculate Tax on Salary
	Then the result should be <expectedResult>

	Examples:
    | amount     | expectedResult |
    | 0		     | 0              |
    | 100	     | 0              |
    | 18200      | 0              |
    | 18201      | 1              |
    | 32567.99   | 2730           |
    | 37000      | 3572           |
    | 37001      | 3573           |
    | 87000      | 19822          |
    | 87001      | 19823          |
    | 180000     | 54232          |
    | 180001     | 54233          |
    | 8000000    | 3573232        |
    | 1234567.33 | 528788         |


@tax
Scenario: Generate error when Salary is a negative value
    Given the amount of -1
    When I try to calculate Tax on Salary
    Then an error message "Salary cannot be a negative value" is received
