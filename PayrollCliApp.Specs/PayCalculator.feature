﻿Feature: PayCalculator
    In order to claculate the net salary
    As a Payroll Manager
    I want to be able to calculate the Medicare Levy on Taxable Income

@payroll
Scenario Outline: Calculate Payroll components on different salary amounts
    Given the salary of <salary>
    When I calculate the Payroll
    Then the "Salary" should be <salary>
    And the "TaxableIncome" should be <taxableIncome>
    And the "MedicareLevy" should be <medicareLevy>
    And the "Superannuation" should be <superannuation>
    And the "BudgetRepairLevy" should be <budgetRepairLevy>
    And the "TaxOffset" should be <taxOffset>
    And the "NetSalary" should be <netSalary>

    Examples:
    | salary    | taxableIncome | incomeTax | superannuation | medicareLevy | budgetRepairLevy | taxOffset | netSalary  |
    | 200,000   | 182,648.40    | 55,424    | 17,351.60      | 3,653        | 53               | 0         | 123,518.40 |
    | 8,000,000 | 7,305,936     | 3,260,904 | 694,064        | 146,119      | 142,519          | 0         | 3,756,394  |
    | 10,000    | 9,132.42      | 0         | 867.58         | 0            | 0                | 445       | 8,687.42   |

@payroll
Scenario: Generate error when Amount is a negative value
    Given the amount of -1
    When I try to calculate Payroll on Salary
    Then an error message "Salary cannot be a negative value" is received
