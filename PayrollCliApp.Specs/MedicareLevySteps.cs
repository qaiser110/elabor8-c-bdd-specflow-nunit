﻿﻿using System;
using TechTalk.SpecFlow;
using NUnit.Framework;
using PayrollCliApp;

namespace PayrollCliApp.Spec
{
    [Binding]
    public class MedicareLevySteps
    {
        [When(@"I calculate Medicare Levy on Taxable Income")]
        public void WhenICalculateMedicareLevyOnTaxableIncome()
        {
            decimal amount = (decimal)ScenarioContext.Current["amount"];
            ScenarioContext.Current["result"] = PayCalculator.CalculateMedicareLevy(amount);
        }

        [When(@"I try to calculate Medicare Levy on Salary")]
        public void WhenITryToCalculateMedicareLevyOnSalary()
        {
            decimal amount = (decimal)ScenarioContext.Current["amount"];
            ScenarioContext.Current["error"] =
                Assert.Throws<ArgumentException>(() => PayCalculator.CalculateMedicareLevy(amount));

        }

    }
}
