﻿Feature: MedicareLevy
    In order to claculate the net salary
    As a Payroll Manager
    I want to be able to calculate the Medicare Levy on Taxable Income

@tax
Scenario Outline: Calculate Income Tax on different Salary amounts
    Given the amount of <amount>
    When I calculate Medicare Levy on Taxable Income
    Then the result should be <expectedResult>

    Examples:
    | amount    | expectedResult |
    | 21335     | 0              |
    | 26669     | 534            |
    | 700500.89 | 14011          |
    | 21336     | 1              |
    | 26667.89  | 534            |

@tax
Scenario: Generate error when Amount is a negative value
    Given the amount of -1
    When I try to calculate Medicare Levy on Salary
    Then an error message "Negative Amount is not allowed" is received
