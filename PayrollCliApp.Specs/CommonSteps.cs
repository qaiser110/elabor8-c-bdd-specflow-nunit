﻿﻿using System;
using TechTalk.SpecFlow;
using NUnit.Framework;
using PayrollCliApp;

namespace PayrollCliApp.Spec
{
    [Binding]
    public class CommonSteps
    {
        [Given(@"the salary of (.*)")]
        [Given(@"the amount of (.*)")]
        public void GivenTheAmountOf(decimal input)
        {
            ScenarioContext.Current["amount"] = input;
        }

        [Then(@"the result should be (.*)")]
        public void ThenTheResultShouldBe(decimal expected)
        {
            Assert.AreEqual(expected, ScenarioContext.Current["result"]);
        }

        [Then(@"an error message ""(.*)"" is received")]
        public void ThenAnErrorMessageIsReceived(string message)
        {
            Exception error = (Exception)ScenarioContext.Current["error"];
            Assert.That(error.Message, Is.EqualTo(message));
        }

    }
}
