﻿Feature: Superannuation
    In order to claculate the net salary
    As a Payroll Manager
    I want to be able to calculate the Superannuation on Taxable Income

@tax
Scenario Outline: Calculate Superannuation on different amounts
    Given the amount of <amount>
    When I calculate Superannuation on Taxable Income
    Then the result should be <expectedResult>

    Examples:
    | amount      | expectedResult   |
    | 100         | 8.6758           |
    | 7000000     | 607306           |
    | 8999999.999 | 780821.999913242 |

@tax
Scenario: Generate error when Amount is a negative value
    Given the amount of -1
    When I try to calculate Superannuation on Income
    Then an error message "Negative Amount is not allowed" is received
