﻿// ------------------------------------------------------------------------------
//  <auto-generated>
//      This code was generated by SpecFlow (http://www.specflow.org/).
//      SpecFlow Version:2.1.0.0
//      SpecFlow Generator Version:2.0.0.0
// 
//      Changes to this file may cause incorrect behavior and will be lost if
//      the code is regenerated.
//  </auto-generated>
// ------------------------------------------------------------------------------
#region Designer generated code
#pragma warning disable
namespace PayrollCliApp.Specs
{
    using TechTalk.SpecFlow;
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("TechTalk.SpecFlow", "2.1.0.0")]
    [System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [NUnit.Framework.TestFixtureAttribute()]
    [NUnit.Framework.DescriptionAttribute("PayCalculator")]
    public partial class PayCalculatorFeature
    {
        
        private TechTalk.SpecFlow.ITestRunner testRunner;
        
#line 1 "PayCalculator.feature"
#line hidden
        
        [NUnit.Framework.TestFixtureSetUpAttribute()]
        public virtual void FeatureSetup()
        {
            testRunner = TechTalk.SpecFlow.TestRunnerManager.GetTestRunner();
            TechTalk.SpecFlow.FeatureInfo featureInfo = new TechTalk.SpecFlow.FeatureInfo(new System.Globalization.CultureInfo("en-US"), "PayCalculator", "    In order to claculate the net salary\r\n    As a Payroll Manager\r\n    I want to" +
                    " be able to calculate the Medicare Levy on Taxable Income", ProgrammingLanguage.CSharp, ((string[])(null)));
            testRunner.OnFeatureStart(featureInfo);
        }
        
        [NUnit.Framework.TestFixtureTearDownAttribute()]
        public virtual void FeatureTearDown()
        {
            testRunner.OnFeatureEnd();
            testRunner = null;
        }
        
        [NUnit.Framework.SetUpAttribute()]
        public virtual void TestInitialize()
        {
        }
        
        [NUnit.Framework.TearDownAttribute()]
        public virtual void ScenarioTearDown()
        {
            testRunner.OnScenarioEnd();
        }
        
        public virtual void ScenarioSetup(TechTalk.SpecFlow.ScenarioInfo scenarioInfo)
        {
            testRunner.OnScenarioStart(scenarioInfo);
        }
        
        public virtual void ScenarioCleanup()
        {
            testRunner.CollectScenarioErrors();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Calculate Payroll components on different salary amounts")]
        [NUnit.Framework.CategoryAttribute("payroll")]
        [NUnit.Framework.TestCaseAttribute("200,000", "182,648.40", "55,424", "17,351.60", "3,653", "53", "0", "123,518.40", new string[0])]
        [NUnit.Framework.TestCaseAttribute("8,000,000", "7,305,936", "3,260,904", "694,064", "146,119", "142,519", "0", "3,756,394", new string[0])]
        [NUnit.Framework.TestCaseAttribute("10,000", "9,132.42", "0", "867.58", "0", "0", "445", "8,687.42", new string[0])]
        public virtual void CalculatePayrollComponentsOnDifferentSalaryAmounts(string salary, string taxableIncome, string incomeTax, string superannuation, string medicareLevy, string budgetRepairLevy, string taxOffset, string netSalary, string[] exampleTags)
        {
            string[] @__tags = new string[] {
                    "payroll"};
            if ((exampleTags != null))
            {
                @__tags = System.Linq.Enumerable.ToArray(System.Linq.Enumerable.Concat(@__tags, exampleTags));
            }
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Calculate Payroll components on different salary amounts", @__tags);
#line 7
this.ScenarioSetup(scenarioInfo);
#line 8
    testRunner.Given(string.Format("the salary of {0}", salary), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 9
    testRunner.When("I calculate the Payroll", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 10
    testRunner.Then(string.Format("the \"Salary\" should be {0}", salary), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 11
    testRunner.And(string.Format("the \"TaxableIncome\" should be {0}", taxableIncome), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 12
    testRunner.And(string.Format("the \"MedicareLevy\" should be {0}", medicareLevy), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 13
    testRunner.And(string.Format("the \"Superannuation\" should be {0}", superannuation), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 14
    testRunner.And(string.Format("the \"BudgetRepairLevy\" should be {0}", budgetRepairLevy), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 15
    testRunner.And(string.Format("the \"TaxOffset\" should be {0}", taxOffset), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 16
    testRunner.And(string.Format("the \"NetSalary\" should be {0}", netSalary), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Generate error when Amount is a negative value")]
        [NUnit.Framework.CategoryAttribute("payroll")]
        public virtual void GenerateErrorWhenAmountIsANegativeValue()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Generate error when Amount is a negative value", new string[] {
                        "payroll"});
#line 25
this.ScenarioSetup(scenarioInfo);
#line 26
    testRunner.Given("the amount of -1", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 27
    testRunner.When("I try to calculate Payroll on Salary", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 28
    testRunner.Then("an error message \"Salary cannot be a negative value\" is received", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            this.ScenarioCleanup();
        }
    }
}
#pragma warning restore
#endregion
