﻿using System;
using TechTalk.SpecFlow;
using NUnit.Framework;
using PayrollCliApp;

namespace PayrollCliApp.Specs
{
    [Binding]
    public class TaxOffsetSteps
    {
        [When(@"I calculate TaxOffset on Taxable Income")]
        public void WhenICalculateTaxOffsetOnTaxableIncome()
        {
            decimal amount = (decimal)ScenarioContext.Current["amount"];
            ScenarioContext.Current["result"] = PayCalculator.CalculateTaxOffset(amount);
        }
        
        [When(@"I try to calculate TaxOffset on Income")]
        public void WhenITryToCalculateTaxOffsetOnIncome()
        {
            decimal amount = (decimal)ScenarioContext.Current["amount"];
            ScenarioContext.Current["error"] =
                Assert.Throws<ArgumentException>(() => PayCalculator.CalculateTaxOffset(amount));
        }
    }
}
