### Additional Problem

If the software was going to be internationalised with different tax rules and deductions, how would your test strategy would change?

There could be different solutions to this problem, depending on the scale of the change. For instance, if the internationalisation involves only a few countries, we can make the PayCalculator, an abstract or a concrete base class, so that country specific classes (for eg. AuPayCalculator, UsaPayCalculator) can inherit from it, and override th necessary methods.

The PaySlipPrinter would also be modified to print in the location specific format.

The Specflow tests would also have to be modified accordingly.
